import db_models from '../db_manage/models'

const main = async () => {
  let saved_actions = await db_models.actions.findAll({});
  for(let row of saved_actions){
    let origin_data = JSON.parse(row.origin_data);

    origin_data = origin_data.traces ? origin_data.traces[0] : origin_data;
    console.log(origin_data);
    let {_form, to, bpname, voter} = origin_data.act.data;
    row._from = _form || '',
    row.to = to || '',
    row.bpname = bpname || '',
    row.voter = voter || '';

    console.log(`${_form}, to is ${to}, bpname is ${bpname}, voter is ${voter}`);
    await row.save();
  }
}

main();