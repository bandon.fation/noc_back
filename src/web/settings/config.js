const mode = process.env.NODE_ENV == 'development' ? 'development' : 'production';
// 同步节点信息
export const eos_node_configs = {
  development: {
    rpc_host: 'http://192.168.82.196:8888',
    rpc_host: 'http://13.115.2.220',
    rpc_host: 'http://47.111.173.249:15001',
  },
  production: {
    rpc_host: 'http://47.111.173.249:15001',
  }
}

export const eos_node_config = eos_node_configs[mode]
