import { 
    get_model,
    excute_sql,
    model_filter
} from '../../db_manage/models.js'

import { log, minus_set } from '../../utils/index.js'

import {
    sync_data_api
} from '../settings/common_eos_api.js'

import {
    eos_dapp_reward_config
} from '../settings/config.js'

import Eos from 'eosforcejs'

export const get_transactions = async (offset, limit) => {
    let start_time = new Date().getTime();
    let sql = `select * from actions order by block_time desc limit ${limit} offset ${offset}`;
    let items = await excute_sql(sql);
    let end_time = new Date().getTime();
    log(`transaction items used time ${end_time - start_time}`);
    return items || [];
}

export const get_transaction_total = async (account_name) => {
    let sql = `select count(*) as total from actions where name != 'onblock'`;
    let res = await excute_sql(sql);
    return res.length ? res[0].total : 0;
}

export const get_total_block_list = async () => {
    let start_time = new Date().getTime();
    let sql = 'select block_num from blocks order by block_num desc limit 1;'
    let items = await excute_sql(sql);
    return items.length ? items[0].block_num : 0;
}

export const get_block_list = async (limit, offset, ...args) => {
    let start_time = new Date().getTime();
    let sql = `select * from blocks order by block_num desc limit ${limit} offset ${offset} `;
    let items = await excute_sql(sql);
    let end_time = new Date().getTime();
    log(`block query used time ${end_time - start_time}`);
    return items;
}

export const get_accounts_info = async (account_name) => {
    let db_accounts = await excute_sql(`select * from accounts where name ="${account_name}"`);
    return db_accounts[0];
}

export const get_total_fixed_vote = async (account_name) => {
  let sql = `select sum(vote) as total_fixed_vote from fixed_vote where voter='${account_name}' and typ > -1 and is_deleted=false`;
  let [ total_fixed_vote ] = await excute_sql(sql);
  total_fixed_vote = total_fixed_vote ? total_fixed_vote.total_fixed_vote : 0;
  return total_fixed_vote;
}

export const get_current_vote = async (account_name) => {
  let sql = `select sum(vote) as current_vote from fixed_vote where voter='${account_name}' and typ = -1 and is_deleted=false`;
  let [{ current_vote }] = await excute_sql(sql);
  current_vote = current_vote ? current_vote : 0;
  return current_vote;
}

export const get_available = async (account_name) => {
  let sql = `select available from account_token where token_type='BAS' and name='${ account_name }';`;
  let [available] = await excute_sql(sql);
  available = available ? available.available : 0;
  return available;
}

export const get_vote_user_num = (account) => {
    return new Promise((resolve, reject) => {
        get_model('transaction')
        .then(({model: transaction_model, db_conn}) => {
            transaction_model.aggregate().distinct('voter').get((err, res) => {
                transaction_model._db_close();
                resolve(res.length);
            });
        });        
    });
}

export const get_bps_from_db = () => {
    return new Promise(async (resolve, reject) => {
        let {model} = await get_model('bps');
        console.log('get_bps_from_db');
        model.find({}, {order: ['update_time', 'Z']}).limit(1).run((err, res) => {
            model._db_close();
            resolve(res[0]);
        });
    });
}

export const search_eos_key = async (key_word) => {
    let sql = `select * from public_key_account where public_key = "${key_word}" limit 1`;
    let query_res = await excute_sql(sql);
    query_res = query_res.length > 0 ? query_res : null;
    return query_res;
}

export const search_accounts = async (key_word) => {
    let start_time = new Date().getTime();

    let res = await model_filter('accounts', {name: key_word});
    res = res.length ? res : null;
    let end_time = new Date().getTime();

    console.log( `search_accounts ${end_time - start_time} is wasted while search transaction` );
    
    return res;
}

export const query_table_by_condition = async (table, table_column, key_word) => {
    let sql = `select * from ${table} where \`${table_column}\` = "${key_word}"`;
    let items = await excute_sql(sql);
    return items.length ? JSON.stringify(items) : null;
}

export const transfer_dapp_reward_eosc = async (to, ammount, memo = '') => {
    let _from = eos_dapp_reward_config.user_name;
    let config = Object.assign({}, eos_dapp_reward_config);
    delete config.user_name;
    let res = await Eos(eos_dapp_reward_config)
    .contract('eosio')
    .then(token => {
        return token.transfer(_from, to, ammount, memo).then(res => res).catch(error => error);
    })
    .catch(error => {
        console.log(error);
    });
    return res;
}

export const multi_transfer_dapp_reward = async (user_name, date_reward, my_dapp_reward) => {
    for(let item of my_dapp_reward){
        if(date_reward.max_limit <= date_reward.has_sended){
            break;
        }
        if(!item.has_revived){
            let send_res = await transfer_dapp_reward_eosc(user_name, '5.0000 EOS', `dapp reward from eos force for the dapp of ${item.dapp_name} 5 EOSC`)
            date_reward.has_sended += 5;
            date_reward.save();
            item.recieved_trx_id = send_res.transaction_id;
            item.recived_time = new Date();
            item.has_revived = true;
            item.save();
        }
        item.has_revived ? null : not_recieved += item.reward;
    }
}

export const get_total_contract = async () => {
    let res = await excute_sql('select count(*) as total from contract_account_analytic');
    return res[0].total;
}

export const get_contract_list = async (limit = 1, offset = 10) => {
    let list = await excute_sql(`select * from contract_account_analytic order by id limit ${limit} offset ${offset}`);
    return list;
}

export const get_contract_info_by_account = async (account) => {
    let item = await excute_sql(`select * from contract_account_analytic where contract_name = "${account}"`);
    return item.length > 0 ? item[0] : null;
}

export const get_total_tokens = async () => {
    let res = await excute_sql('select count(*) as total from token_info');
    return res[0].total;
}

export const get_token_info_by_symbol = async (symbol) => {
    let item = await excute_sql(`select * from token_info where symbol = "${symbol}"`);
    return item.length > 0 ? item[0] : null;
}

export const get_token_list = async (limit = 1, offset = 10) => {
    let list = await excute_sql(`select * from token_info order by id limit ${limit} offset ${offset}`);
    return list;
}

export const get_analytics_by_key = async (key_name) => {
    let start_time = new Date().getTime();
    let row = await excute_sql(`select value from analytics where \`key\` = '${key_name}' `);
    let result = row.length ? row[0].value : 0;
    return result;
}

export const query_transaction_by_block_time = async ({key = 'actor', value = 'eosio', offset = 0, limit = 20, pre_id = -1}) => {
    let sql = `select id from actions where \`${key}\` in ("${value}") and id < ${pre_id < 0 ? 10 ** 100 : pre_id} order by block_time desc limit ${limit} offset ${offset}`;
    let rows = await excute_sql(sql);
    return rows;
}









