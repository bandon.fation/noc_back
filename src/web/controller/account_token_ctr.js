import Express from 'express'

import { log, is_int_number } from '../../utils/index.js'

import { 
    get_transaction_total,
    get_bps_from_db,
    get_total_tokens,
    get_token_list,
    get_contract_info_by_account,
    get_token_info_by_symbol
} from '../models'

import {
    sync_data_api
} from '../settings/common_eos_api.js'

var router = Express.Router();

router.post('/web/get_token_list', async (req, res) => {
    /*
        params:
            page int
            step int

        response
            {
                status: 0
                data: {
                    total: int
                    page_num: int
                    items: Array
                }
            }
    */
    let form = req.body,

        {page = 1, step = 10} = form,

        base_res = {
            status: 0,
            msg: ''
        };

    if (!is_int_number(page) || !is_int_number(step)) {
        base_res.status = 2;
        base_res.msg = 'page and step must be integer number';
        res.send(base_res);
    }
    let offset = (page - 1) * step;
    let [total, items] = await Promise.all([get_total_tokens(), get_token_list(step, offset)]);
    res.send({
        status: 0,
        data: {
            total,
            total_page_num: Math.ceil(total/step),
            page_num : page,
            items
        }
    });
    
});

router.post('/web/get_token_info_by_symbol', async (req, resp) => {
    /*
    params:
        symbol string
    */
    let form = req.body,
        {symbol} = form,
        base_res = {
            status: 0,
            msg: ''
        };
    if(!symbol){
        base_res.status = 2;
        base_res.msg = 'no symbol';
        resp.send(base_res);
        return ;
    }

    base_res.contract_info = await get_token_info_by_symbol(symbol);
    resp.send(base_res);
});

export default router;
