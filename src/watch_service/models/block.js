import Eos from 'eosforcejs'
import config from '../../config'
import db_models from '../../db_manage/models'
import {
  consum,
  send_msg
} from '../../mq_manage'
import {
  get_lastest,
  update_or_create_model
} from '../../db_manage/models'
import {
  wait_time
} from '../../utils/index'

import {
  update_account_info
} from './accounts'

import {
  query_available,
  update_users_token_available,
  update_account
} from './update_tables'

import rmq_config from '../config'

const {eos_node} = config;
const EOS = Eos(eos_node);
const BLOCK_NUMS = [];

const check_confirmed_block = async (block_num) => {
  let saved_block = await db_models.blocks.findAll({where: {block_num, is_confirmed: true}});
  if(saved_block.length){
    return saved_block[0];
  }
  return null;
}

const curl_ans_save_block = async (block_ob, confirmed_block, rmq_config) => {
  let { block_num, is_confirmed } = block_ob;

  console.log(block_num, 'curl_ans_save_block');
  // let confirmed_block = await check_confirmed_block(block_num);
  // if(confirmed_block) return ;
  if(confirmed_block) return;
  let block = await EOS.getBlock(block_num);
  confirmed_block = await check_confirmed_block(block_num);
  if(confirmed_block) return ;

  if(!JSON.stringify(block)){
    console.log('error');
    console.log(block);
    throw Error('block is error');
  }

  block.is_confirmed = is_confirmed;

  send_msg({
    'msg_type': 'exchange',
    'exchange_name': rmq_config.name,
    'msg': JSON.stringify(block)
  });

  block.origin_data = JSON.stringify(block);
  if(block.producer){
    await update_account_info({
      account_name: [block.producer]
    });
  }
  console.log(`block producer is ${ block.producer }, block num is ${ block.block_num }`)
  // send msg to transaction exchange
  
  block.block_id = block.id;
  delete block.id;
  block.new_producers = '';
  block.header_extensions = '';
  block.block_extensions = JSON.stringify(block.block_extensions);
  block.transaction_length = block.transactions.length;
  block.transactions = '';
  block.is_confirmed = is_confirmed;
  let res = await update_or_create_model('blocks', 'block_num', block);
  if(block_num == 1515607){
    throw Error('dsd')
  }
  return res;
}

const curl_block = async (block_nums = [], rmq_config) => {
  let processing = [];
  let saved_blocks = await db_models.blocks.findAll({where: {block_num: block_nums.map(i => i.block_num), is_confirmed: true}});
  // console.log(saved_blocks);
  for(let i of block_nums){
    let is_confirmed = saved_blocks.find(si => si.block_num == i.block_num && si.is_confirmed)
    // console.log(`is_confirmed is ${i}, ${is_confirmed.block_num}`)
    processing.push( curl_ans_save_block(i, is_confirmed, rmq_config));
  }
  let blocks = await Promise.all(processing);
}

export const processing_blocks = async ({block_nums = [], step = 2, save_key = 'max_saved_block_num', update_analytic_key = true, rmq_config = {}}) => {
  for (var i = 0; i <= block_nums.length; i+= step) {
    let process_block_nums = block_nums.slice(i, step + i);
    if(!process_block_nums.length){
      continue ;
    }
    try{
      await curl_block(process_block_nums, rmq_config);  
    }catch(error){
      console.log('end ----------------->');
      console.log(error);
      throw Error('blocks updated error')
    }
    let latest_block_num = process_block_nums[ process_block_nums.length - 1 ].block_num;
    if(!update_analytic_key) return ;
    await update_or_create_model('analytics', 'key', {
      key: save_key,
      value: latest_block_num
    });
  }
}

