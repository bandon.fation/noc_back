import Eos from 'eosforcejs'
const {eos_node} = config;
const EOS = Eos(eos_node);
const BLOCK_NUMS = [];

const query_abis = async (accounts) => {
  let ps = [];
  for(let i of accounts){
    ps.push( EOS.getAbi(i) );
  }
  let result = await Promise.all(ps);
  return result;
}

const get_type_key = async (action, type = 'account_name') => {
  let abis = await query_abis([action.act.account]), 
        {structs, tables, actions} = abis[0].abi,
        action_format = structs.find(i => i.name == action.act.name);
  let account_keys = [], result = [];
  action_format.fields.forEach(item => {
    if(item.type == type){
      result.push( action.act.data[item.name] );
    }
    if(item.type == type){
      result.splice(result.length, 0, ...action.act.data[item.name]);
    }
  });
  result = [...new Set(result)];
  return result;
}