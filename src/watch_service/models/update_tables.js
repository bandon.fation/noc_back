import Eos from 'eosforcejs'
import config from '../../config'
import db_models from '../../db_manage/models'
import {
  EOS
} from './abi_utils'

export const query_available = async ({account_name = '', eos = EOS, token_type = 'BACC'}) => {
  let params = {
    "scope": account_name,
    "code": "bacc.token",
    "table": "accounts",
    // "type": "name",
    "lower_bound": token_type,
    "limit": 1,
    "json": true
  };
  let data = await eos.getTableRows(params);
  data.account_name = account_name;
  return data;
}

export const update_account_token = async ({name, token_type, available}) => {
  if(!name || !token_type || typeof available != 'number') throw Error('update account token params must be [name: string, token_type: string, available: number]')
  const unique_keys = ['name', 'token_type'];
  const saved_account_token = await db_models.account_token.findAll({where: { name, token_type }})

  const account_token_info = {
    name,
    token_type,
    available
  }

  if(saved_account_token.length){
    let new_row = Object.assign(saved_account_token[0], account_token_info);
    await new_row.save();
  }else{
    await db_models.account_token.create(account_token_info);
  }

}

export const update_users_token_available = async (account_names = [], token_type = 'BACC') => {
  let ps = [],
      ps_params = [];
  for(let name of account_names){
    // let account_tokens = await db_models.account_token.findAll({where: { name, token_type }});
    // for(let index in account_tokens){
    //   let row = account_tokens[index];
    //   row.available = 0;
    //   await row.save()
    // }

    ps.push( query_available({account_name: name, token_type}) );
    ps_params.push({name, token_type})
  }

  let datas = await Promise.all(ps);

  let update_account_info_process = [];
  datas.forEach(async (item, index) => {
    if(!item.rows.length){
      console.log('ps_params[index]');
      console.log(ps_params[index]);
      let account_tokens = await db_models.account_token.findAll({where: ps_params});
      if(account_tokens.length > 0){
        account_tokens.available = 0;
        await account_tokens.save();
      }
    }

    item.rows.forEach(async row => {
      let [available, token_type] = row.balance.split(' ')
      let process = update_account_token({
        name: item.account_name,
        token_type,
        available: parseFloat(available) 
      });
      update_account_info_process.push( process );
    })
  });

  await Promise.all(update_account_info_process);

  // throw Error('test');
  return datas;
}
