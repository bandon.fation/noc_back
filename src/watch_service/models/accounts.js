import db_models from '../../db_manage/models'
import {
  EOS
} from './abi_utils'
import {
  excute_sql,
  update_or_create_model
} from '../../db_manage/models'

export const update_accounts_num = async () => {
  let count = await excute_sql('select count(*) as ACCOUNT_NUM  from accounts');
  count = count[0].ACCOUNT_NUM;
  await update_or_create_model('analytics', 'key', {
    key: 'ACCOUNT_NUM',
    value: count
  });
}

export const update_contracts_num = async () => {
  // contract_account_analytic
  let count = await excute_sql('select count(*) as CONTRACT_ACCOUNT_NUM  from contract_account_analytic');
  count = count[0].CONTRACT_ACCOUNT_NUM;
  await update_or_create_model('analytics', 'key', {
    key: 'CONTRACT_ACCOUNT_NUM',
    value: count
  });
}

export const update_contracts_actions_by_account = async (account) => {
  const sql = `select distinct(\`name\`) from actions where account="${account}";`;
  let result = await excute_sql(sql);

  let query_action_num = `select count(*) as actions_num from actions where account="${account}";`;
  let actions_num = await excute_sql(query_action_num);
  actions_num = actions_num.length ? actions_num[0].actions_num : 0;
  console.log(actions_num, 'actions_num');
  // contract_account_analytic
  await update_or_create_model('contract_account_analytic', 'contract_name', 
    {
      contract_name: account,
      actions: result.map(i => i.name).join(','),
      transaction_num: actions_num
    }
  );
}

/*
  检查用户基础数据是否已存在
*/
export const update_account_info = async ({account_name = '', eos = EOS, force = false}) => {
  account_name = account_name.splice && account_name.join ? account_name : [account_name];
  const account_names = [...new Set(account_name)];

  const exist_accounts = await db_models.accounts.findAll({where: {name: account_names}})
  if(exist_accounts.length == account_names.length && !force) return exist_accounts;
  let ps = [], not_save_accounts = [];

  account_names.forEach(account_name => {
    ps.push(
      eos.getAccount({account_name})
      .catch(error => {
        return false;
      })
    );
  });

  let data = await Promise.all(ps), result = [];

  for(let account of account_names){

    let saved_account = exist_accounts.find(row => row.name == account);
    const chain_account_info = data.find(item => item.account_name == account);
    if(!chain_account_info) continue;
    const account_info = {
      name: chain_account_info.account_name,
      available: 0,
      created: chain_account_info.created,
      last_code_update: chain_account_info.last_code_update,
      unlock_time: new Date(),
      staked: 0,
      unstaking: 0,
      last_update_time: new Date(),
      origin_data: JSON.stringify(chain_account_info),
      creator: null,
    }

    let updated_row = saved_account;

    if(updated_row){
      let is_changed = false;
      if(updated_row.last_code_update != chain_account_info.last_code_update){
        updated_row.last_code_update = chain_account_info.last_code_update;
        is_changed = true;
      }
      if(updated_row.last_update_time != chain_account_info.last_update_time){
        updated_row.last_update_time = chain_account_info.last_update_time;
        is_changed = true;
      }
      if(updated_row.origin_data != chain_account_info.origin_data){
        updated_row.origin_data = chain_account_info.origin_data;  
        is_changed = true;
      }
      
      if(is_changed){
        updated_row.save()
      }
      // updated_row = Object.assign(saved_account, account_info);
      
    }else{
      updated_row = await db_models.accounts.create(account_info);
    }

    result.push( updated_row );
  }

  return result;
}

/*
  name: 
  update_block_num: 涉及更新的区块高度，包含[updateauth, newaccount]交易的区块
*/
export const update_account_public_key = async (name, block_num, exist_account = null) => {
  console.log('update_account_public_key-s')
  exist_account = exist_account ? exist_account : await db_models.accounts.findAll({where: {name}});
  console.log('update_account_public_key-e')
  exist_account = exist_account.length ? exist_account[0] : exist_account;
  const origin_data = JSON.parse(exist_account.origin_data);
  let key_accounts = [];
  origin_data.permissions.forEach(row => {
    let {perm_name} = row;
    row.required_auth.keys.forEach(key_item => {
      let {key: public_key, weight} = key_item;
      key_accounts.push({
        name,
        perm_name,
        public_key,
        weight,
        is_deleted: false,
        update_block_num: block_num
      })
    });
  });

  let exist_key_accounts = await db_models.public_key_account.findAll({where: {name}});

  for(let item of exist_key_accounts){
    item.is_deleted = true;
    item.save();
  }

  for(let item of key_accounts){
    // 'name', 'public_key', 'perm_name', 'is_deleted', 'update_block_num'
    // await 
    let existed_item = exist_key_accounts.find(es => es.name == item.name && es.public_key == item.public_key && es.perm_name == item.perm_name && es.update_block_num == item.update_block_num);
    if(existed_item){
      existed_item.is_deleted = false;
      existed_item.save();
    }else{
      db_models.public_key_account.create(item);
    }
  }

}


