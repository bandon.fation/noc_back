import Eos from 'eosforcejs'
import config from '../config'
import db_models from '../db_manage/models'
import {
  consum,
  send_msg
} from '../mq_manage'
import {
  get_lastest,
  update_or_create_model
} from '../db_manage/models'
import {
  wait_time
} from '../utils/index'
import {
  get_value_by_type
} from './models/abi_utils'
import rmq_config from './config'

const {eos_node} = config;
const EOS = Eos(eos_node);

const filter_and_save_action_from_trace = async (trxs = [], block_data = {}) => {
  let actions = [], origin_actions = [];
  // console.log(trxs);
  trxs.forEach(row => {
    for(let i = 0; i < row.traces.length; i++){
      let ob = row.traces[i];
      origin_actions.push(ob);
      // let assets = await get_value_by_type(ob, 'asset')
      // console.log(assets);
      // throw Error('{')
      let new_ob = {
        account: ob.act.account,
        name: ob.act.name,
        block_time: ob.block_time,
        trx_id: ob.trx_id,
        _from: ob.act.data.from,
        to: ob.act.data.to,
        bpname: ob.act.data.bpname || '',
        quantity: ob.act.data.quantity ? ob.act.data.quantity.split(' ')[0] : null,
        stake: ob.act.data.stake ? ob.act.data.stake.split(' ')[0] : null,
        voter: ob.act.data.voter ? ob.act.data.voter : null,
        token_type: ob.act.name == 'transfer' ? ob.act.data.quantity.split(' ')[1] : null,
        memo: ob.act.data.memo ? ob.act.data.memo : '',
        actor: ob.act.authorization[0].actor,
        block_num: ob.block_num,
        origin_data: JSON.stringify(ob),
        permission: ob.act.authorization[0].permission,
        global_sequence: ob.receipt.global_sequence,
        is_confirmed: block_data.is_confirmed
      }
      actions.push(new_ob);
      if(row.traces[i].act.name == 'transfer'){
        i += 2
      }
    }
  });

  // 确认后，删除的未确认交易
  const deleted_actions = [];
  if(block_data.is_confirmed){
    let saved_actions = await db_models.actions.findAll({
      where: {block_num: block_data.block_num}
    });

    // 获取未确认的交易
    saved_actions.forEach(row => {
      if(actions.find(i => i.trx_id != row.trx_id)){
        deleted_actions.push(JSON.parse(row.origin_data));
      }
    });

    let max_len = actions.length >= saved_actions.length ? actions.length : saved_actions.length;

    for (var i = 0; i < max_len; i++) {
      if(saved_actions[i] && actions[i]){
        Object.assign(saved_actions[i], actions[i]);
        saved_actions[i].save();
      }
      if(saved_actions[i] && !actions[i]){
        saved_actions[i].is_confirmed = -1;
        saved_actions[i].save();
      }
      if(!saved_actions[i] && actions[i]){
        db_models.actions.create( actions[i] );
      }
    }
  }else{
    for(let i of actions){
      db_models.actions.create(i);
    }
  }
  // 
  let result = {
    actions: origin_actions,
    deleted_actions,
    is_confirmed: block_data.is_confirmed
  }

  if(!origin_actions.length && !deleted_actions.length) {
    console.log(`watch transaction skip block num is ${ block_data.block_num }, ${ block_data.is_confirmed }-------------------------`)
    return result;
  }
  console.log(`not skip block num is ${ block_data.block_num }, ${ block_data.is_confirmed }-------------------------`)
  send_msg({
    'msg_type': 'exchange',
    'exchange_name': rmq_config.action_exchange.name,
    'msg': JSON.stringify(result)
  });
  return result;
}

export const curl_transactions = async (block_data) => {
  let promise = [];
  block_data.transactions.forEach(row => {
    let trx_id = typeof row.trx == 'string' ? row.trx : row.trx.id;
    console.log( 'row' );
    // console.log( row.trx.transaction.actions );
     promise.push( 
      EOS.getTransaction(trx_id)
    );
  });
  let trxs = await Promise.all(promise);
  await filter_and_save_action_from_trace(trxs, block_data);  
}

const consum_data = async (rmq_config) => {
  consum({
    'bind_queue': rmq_config.transaction_queue ,
    'msg_type': 'exchange',
    'exchange_name': rmq_config.name,
    'prefetch': 1,
    async call_back (msg, channel) {
      let block = JSON.parse(msg.content.toString());
      console.log(`start_block_num is ${block.block_num} and ${block.is_confirmed}`)
      if(!block.transactions.length && !block.is_confirmed){
        console.log(`skip ${ block.block_num }`)
        channel.ack(msg);
        return ;
      }
      try{
        await curl_transactions(block)
        channel.ack(msg);
      }catch(error){
        channel.nack(msg, true);
        throw error;
      }
    }
  })
}

const main = async () => {
  for(let i of new Array(6).keys()){
    consum_data(rmq_config.block_exchange);  
  }
  for(let i of new Array(6).keys()){
    consum_data(rmq_config.block_exchange_for_confirm);  
  }
}

main();


