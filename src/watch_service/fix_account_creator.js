import Eos from 'eosforcejs'

import config from '../config'

const { eos_node } = config;
export const EOS = Eos(eos_node);


import db_models from '../db_manage/models'

console.log(eos_node);

import {
  processing_blocks 
} from './models/block'

import {
  query_available,
  update_users_token_available,
  update_account
} from './models/update_tables'

import {
  update_account_info,
  update_account_public_key,
  update_accounts_num,
  update_contracts_num,
  update_contracts_actions_by_account
} from './models/accounts'

import {
  consum,
  send_msg
} from '../mq_manage'

import {
  get_lastest,
  update_or_create_model,
  excute_sql
} from '../db_manage/models'

import {
  wait_time
} from '../utils/index'

import {
  get_value_by_type
} from './models/abi_utils'

import {
  update_transaction_num
} from './models/actions'

import {
  update_vote_by_account,
  update_vote_users_num
} from './models/vote'

import rmq_config from './config'

const filter_and_save_action_from_trace = async (trxs = [], block_data = {}) => {
  let actions = [], origin_actions = [];
  console.log(trxs);
  trxs.forEach(row => {
    for(let i = 0; i < row.traces.length; i++){
      let ob = row.traces[i];
      origin_actions.push(ob);
      // let assets = await get_value_by_type(ob, 'asset')
      // console.log(assets);
      // throw Error('{')
      let new_ob = {
        account: ob.act.account,
        name: ob.act.name,
        block_time: ob.block_time,
        trx_id: ob.trx_id,
        _from: ob.act.data.from,
        to: ob.act.data.to,
        bpname: ob.act.data.bpname || '',
        quantity: ob.act.data.quantity ? ob.act.data.quantity.split(' ')[0] : null,
        stake: ob.act.data.stake ? ob.act.data.stake.split(' ')[0] : null,
        voter: ob.act.data.voter ? ob.act.data.voter : null,
        token_type: ob.act.name == 'transfer' ? ob.act.data.quantity.split(' ')[1] : null,
        memo: ob.act.data.memo ? ob.act.data.memo : '',
        actor: ob.act.authorization[0].actor,
        block_num: ob.block_num,
        origin_data: JSON.stringify(ob),
        permission: ob.act.authorization[0].permission,
        global_sequence: ob.receipt.global_sequence,
        is_confirmed: block_data.is_confirmed
      }
      actions.push(new_ob);
      if(row.traces[i].act.name == 'transfer'){
        i += 2
      }
    }
  });

  // 确认后，删除的未确认交易
  const deleted_actions = [];
  if(block_data.is_confirmed){
    let saved_actions = await db_models.actions.findAll({
      where: {block_num: block_data.block_num}
    });

    // 获取未确认的交易
    saved_actions.forEach(row => {
      if(actions.find(i => i.trx_id != row.trx_id)){
        deleted_actions.push(JSON.parse(row.origin_data));
      }
    });

    let max_len = actions.length >= saved_actions.length ? actions.length : saved_actions.length;

    for (var i = 0; i < max_len; i++) {
      if(saved_actions[i] && actions[i]){
        Object.assign(saved_actions[i], actions[i]);
        saved_actions[i].save();
      }
      if(saved_actions[i] && !actions[i]){
        saved_actions[i].is_confirmed = -1;
        saved_actions[i].save();
      }
      if(!saved_actions[i] && actions[i]){
        db_models.actions.create( actions[i] );
      }
    }
  }else{
    for(let i of actions){
      db_models.actions.create(i);
    }
  }
  // 
  let result = {
    actions: origin_actions,
    deleted_actions,
    is_confirmed: block_data.is_confirmed
  }

  if(!origin_actions.length && !deleted_actions.length) {
    console.log(`watch transaction skip block num is ${ block_data.block_num }, ${ block_data.is_confirmed }-------------------------`)
    return result;
  }
  console.log(`not skip block num is ${ block_data.block_num }, ${ block_data.is_confirmed }-------------------------`)
  send_msg({
    'msg_type': 'exchange',
    'exchange_name': rmq_config.action_exchange.name,
    'msg': JSON.stringify(result)
  });
  return result;
}

export const curl_transactions = async (block_data) => {
  let promise = [];
  block_data.transactions.forEach(row => {
    let trx_id = typeof row.trx == 'string' ? row.trx : row.trx.id;
    console.log( 'row' );
    console.log(trx_id)
    // console.log( row.trx.transaction.actions );
     promise.push( 
      EOS.getTransaction(trx_id)
    );
  });
  let trxs = await Promise.all(promise);
  console.log('------>')
  console.log(trxs);
  await filter_and_save_action_from_trace(trxs, block_data);  
}
const main = async () => {
  let action_block_nums = await excute_sql('select distinct(block_num) from actions where name ="newaccount"');

  let action_block_nums_dict = {};

  action_block_nums.forEach(item => {
    action_block_nums_dict[item.block_num] = 1;
  });

  console.log(action_block_nums[10]);

  let all_blocks = new Array(2217216);
  let start = 1;

  let not_saved_block_nums = action_block_nums.map(i => i.block_num);

  // for(let i of all_blocks.keys()){
  //   let num = i + 1;
  //   if(start > num){
  //     continue;
  //   }
  //   if(!action_block_nums_dict[num]){
  //     not_saved_block_nums.push(num);
  //   }
  // }

  let not_saved_blocks = [];

  let test_nums = 0, create_numss = 0;

  for(var i = 0; i <= not_saved_block_nums.length - 1; i += 100){
    test_nums ++;
    // if(test_nums > 10){
    //   break;
    // }
    let block_num_list = not_saved_block_nums.slice(i, i + 100);
    let saved_blocks = await  db_models.blocks.findAll({where: { block_num: block_num_list }}) //excute_sql(`select block_num, origin_data from blocks where block_num in (${ block_num_list.join(',') })`)
    // console.log(`select block_num, origin_data from blocks where block_num in (${ block_num_list.join(',') })`);
    // console.log( block_num_list );
    // console.log(saved_blocks);
    let can_curl = [];

    for(let item of saved_blocks){
      try{
        let origin_data = JSON.parse(item.origin_data);
        
        if(origin_data.transactions.length){
          item.is_confirmed = false;
          await item.save();
          origin_data.is_confirmed = true;
          console.log(item.block_num);
          // console.log(origin_data.transactions[0].trx)
          let ps = [];
          for(let trx of origin_data.transactions){
            for(let trx_item of trx.trx.transaction.actions){
              console.log(trx_item.name);
              if(trx_item.name != 'newaccount') continue;
              let {creator, name} = trx_item.data;
              ps.push( update_account_info({
                account_name: [creator, name]
              }) );

              ps.push( update_or_create_model('accounts', 'name', {
                name,
                creator,
                created: origin_data.timestamp,
                create_block_num: origin_data.block_num
              }) );

              ps.push( update_users_token_available(['creator', 'name'], 'BACC') );

              ps.push( update_account_public_key(name, origin_data.block_num) );
              create_numss += 1;
            }
          }

          if(ps.length){
            await Promise.all(ps);
          }
          console.log(`length is ${not_saved_block_nums.length}, index is ${i}, create_numss is ${create_numss}` )
          // can_curl.push(origin_data);
          // can_curl.push( curl_transactions(origin_data) );
          // await curl_transactions(origin_data) ;
          // not_saved_blocks.push( {block_num: item.block_num, is_confirmed: true} );
        }else{
          // console.log(item.block_num);
          console.log('item.block_num not_have');
        }
      }catch(error){

      }
    }

    // if(can_curl.length){
    //   await Promise.all(can_curl);
    // }
    
  }

  // await processing_blocks({block_nums: not_saved_blocks, step: 40, save_key: 'max_saved_key'});
  console.log('not_saved_blocks222------>');
  console.log(not_saved_blocks);
  console.log(not_saved_blocks);
  console.log(not_saved_blocks);
  console.log(not_saved_blocks.length);
  console.log('not_saved_blocks');
  // for(let row of blocks){
  //   let origin_data = JSON.parse( blocks.origin_data );
  //   let trx_ids = [];
  //   origin_data.transactions.forEach(item => {
  //     let trx_id = item.trx.id || item.trx;
  //     trx_ids.push(trx_id);
  //   });
  // }

}

main();