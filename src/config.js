const mode = new Set(['development', 'production']).has(process.env.NODE_ENV) ? process.env.NODE_ENV : 'development';
const eos_node = process.env.eos_node
const config = {
  development: {
    database: {
      host: '0.0.0.0',
      port: '33900',
      db_name: 'noc',
      name: 'root',
      password: ''
    },
    eos_node: {
      keyProvider: 'wif',
      httpEndpoint: 'http://47.98.238.9:15001',
      chainId: ''
    },
    mq: {
      host: '47.244.76.95',
      port: 5672,
      user: 'bd',
      password: 'bd'
    }
  },
  production: {
    database: {
      host: 'noc_back_maria_db',
      db_name: 'noc',
      name: 'root',
      port   : '3306',
      password: '123456'
    },
    eos_node: {
      keyProvider: 'wif',
      httpEndpoint: eos_node || 'http://47.75.200.188:9001',
      chainId: ''
    },
    mq: {
      host: 'rabbitmq',
      port: 5672,
      user: 'bd',
      password: 'bd'
    }
  }
}

export default config['production'];