
import {
  get_connection,
  send_msg,
  consum
} from './index'

const test_get_connection = async () => {
  let conn = await get_connection().catch(error => {
    throw error;
  })
  console.log(conn);
}

// test_get_connection();

const test_send_msg = async () => {

  for(let key of new Array(1000).keys()){
    let msg = new Date().getTime();
    send_msg({
      msg: msg + '',
      exchange_name: 'et_12',
      msg_type: 'exchange'
    });
  }
  
}

// test_send_msg();

const test_consum = async () => {
  consum(
    {
      bind_queue: 'e22', 
      exchange_name: 'et_12', 
      msg_type: 'exchange',
      prefetch: 100,
      call_back (msg, channel) {
        console.log(msg.content.toString());
        channel.ack(msg);
      }
    }
  );
}

// test_consum();