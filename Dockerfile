FROM ubuntu:latest

RUN apt-get update -y --fix-missing
RUN apt-get upgrade -y --fix-missing
RUN apt-get install nodejs -y --fix-missing
RUN apt-get install npm -y --fix-missing
RUN apt-get install wget -y --fix-missing
RUN apt-get install curl -y --fix-missing
RUN apt-get install mysql-server mysql-client -y
RUN apt-get install nginx -y

RUN npm i -g n
RUN n stable
RUN npm i -g babel-node babel-cli babel-core pm2


WORKDIR noc_back
ADD . ./
ADD nginx/nginx.conf /etc/nginx/nginx.conf


ENTRYPOINT ["/bin/bash", "./start_product_in_docker.sh"]

